#!/usr/bin/python2.6
# extract upload info from a debian-devel-changes mailbox file
# By Lucas Nussbaum

# -*- coding: utf-8 -*-

import email
import email.Parser
#lowercased in 2.5
import email.Utils as emailutils
import mailbox
import re
import rfc822
import subprocess
import sys
import os
import cPickle
import gzip

from os.path import basename
from debian import deb822

signedby_RE = re.compile("^Signed-By: (.*)", re.MULTILINE)

bugnumber_list_RE = re.compile("^([0-9]{3,} ?)+")

# don't forget to include a slash to keyring path, otherwise gpg assumes it is in GNUPGHOME
def get_key_info_gpgv(msg, keyring=[
                              "./debian-keyring.gpg",
                              "./debian-maintainers.gpg",
                              "./emeritus-keyring.gpg",
                              "./emeritus-keyring.pgp",
                              "./removed-keys.gpg",
                              "./removed-keys.pgp",
                              ]):
    """ Verify given msg with gpgv using the given (list of) keyring(s)
    
    Return a dictionary with detected fields """

    # note: gpgv emits VALIDSIG also for expired (sub)keys and we rely on this
    # fact to get the fingerprint anyway, gpg --verify seems to act differently
    cmd = ["/usr/bin/gpgv", "--status-fd", "1"] 

    if os.environ.has_key("KEYRING"):
        keyring = os.environ["KEYRING"].split(":")

    opt_keyrings = []
    if isinstance(keyring, str):
        if os.path.exists(keyring):
            opt_keyrings.extend(["--keyring", keyring])
    else:
        for k in tuple(keyring):
            if os.path.exists(str(k)):
                opt_keyrings.extend(["--keyring", str(k)])

    if opt_keyrings:
        cmd.extend(opt_keyrings)
    else:
        raise ValueError, "no keyring found"

    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
            stdout=subprocess.PIPE, env={"LC_ALL": "C", "GNUPGHOME": os.curdir} )

    (out, err) = p.communicate("\n".join(msg))
    if not out:
        return {}
   
    retval = {}
    for l in out.split('\n'):
        if l.startswith("[GNUPG:] NODATA 1"):
            return {}

        # VALIDSIG <space-separated fields> fingerprint
        if l.startswith("[GNUPG:] VALIDSIG"):
            retval['Fingerprint'] = l.split(' ')[-1]

        m = keyid_RE.search(l)
        if m:
            # see /usr/share/doc/gnupg/DETAILS.gz
            # fields with <longuid> <username> format 
            if m.group(1) in ("GOOD", "EXP", "EXPKEY", "REVKEY", "BAD"):
                retval['Key'] = m.group(2)
                retval['Signed-By'] = m.group(3)
            # ERRSIG  <long keyid>  <pubkey_algo> <hash_algo> <sig_class> <timestamp> <rc>
            elif m.group(1) == "ERR":
                retval['Key'] = m.group(2)

    return retval

get_key_info = get_key_info_gpgv

def normalize_date(date):
    d = emailutils.parsedate_tz(date)
    if not d:
        return None
    
    try:
        ret = emailutils.formatdate(emailutils.mktime_tz(d), True)
    except OverflowError:
        ret = None

    return ret

def build_keyring_list(dir):
    """ Scan dir and return a list of every file ending with .gpg or .pgp """
    keyrings = []
    for root, dirs, files in os.walk(dir):
        keyrings.extend([os.path.join(root, x) for x in files if x.endswith(".gpg") or x.endswith(".pgp")])

    return keyrings
              
# XXX factor this to work with plain .changes
def munge_ddc(mb_file, outfile=sys.stdout):
    """ Scan given filename mb_file as a unix mailbox and extract upload informations """
    if mb_file.endswith(".gz"):
        f = gzip.open(mb_file)
    else:
        f = file(mb_file)
    mb = mailbox.PortableUnixMailbox(f, factory=email.message_from_file)

    results = []
    for msg in mb:
        found_signed_by = None
        body = None
        if msg.is_multipart():
            for part in msg.get_payload():
                if part.get_filename() == 'changesfile':
                    body = part.get_payload(decode=True).split('\n')
                else:
                    # Signed-By isn't in the changesfile for some reason
                    body = part.get_payload(decode=True)
                    if not body:
                        continue
                    match = signedby_RE.search(body)
                    if match is not None:
                        found_signed_by = match.group(1)
        else:
            body = msg.get_payload(decode=True).split('\n')

        if body is None:
            continue

        if body[0] == 'Sorry, changesfile not available.': # work around new syncs not having changesfiles
            continue

        # XXX work around Hash: sha1(\n)+ at the beginning of signed body
        for (i,l) in enumerate(body):
            if l.startswith("Format:") or l.startswith("Distribution:"):
                break

        rc = deb822.Changes(body[i:])
        if found_signed_by is not None:
            rc['Signed-By'] = found_signed_by

        c = {}
        c['Signed-By'] = 'N/A'

        c['Message-Id'] = str(msg['Message-Id']).strip('\n')
        c['Message-Date'] = str(msg['Date']).strip('\n')

	if not set(['Source', 'Architecture', 'Version', 'Date', 'Changes']).issubset(rc.keys()):
            sys.stderr.write("Required fields not found, skipping.\n")
            continue
   
        for field in ['Source', 'Architecture', 'Version', 'Date', 'Changed-By', 'Maintainer', 'Original-Maintainer', 'Closes', 'Launchpad-Bugs-Fixed', 'Signed-By', 'Distribution', 'Component']:
            if rc.has_key(field):
                c[field] = rc[field].encode('utf-8')
            else:
                c[field] = 'N/A'
#        if rc.has_key('Maintainer'):
#            try:
#                sys.stderr.write(c['Source']+" "+c['Version']+" "+c['Date']+"\n")

        if not 'source' in c['Architecture']:
            #sys.stderr.write("%s: source not in architecture\n" % pkg)
            continue
        
        if not os.environ.has_key("NO_KEYRING_CHECK"):
            if os.environ.has_key("KEYRING_DIR"):
                c.update(get_key_info(body, keyring = build_keyring_list(os.environ['KEYRING_DIR']) ))
            else:
                c.update(get_key_info(body))

        ### Fields sanitization

        # try first with changes date, then message date
        d = normalize_date(c['Date'])
        if not d:
            c['Date'] = "N/A"

        if c.has_key('Closes'):
            m = bugnumber_list_RE.match(c['Closes'])
            if not m:
                c['Closes'] = "N/A"

        # finally output fields
        for field in c.keys():
            outfile.write("%s: %s" % (field, c[field].strip('\n')))
            # separate different fields
            outfile.write("\n")

        # separate different packages
        outfile.write("\n")

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "usage: file1 .. fileN"
        sys.exit(1)

    for f in sys.argv[1:]:
        munge_ddc(f)

# vim: et:ts=4:sw=4
