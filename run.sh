#!/bin/sh
set -e

BASE=/home/laney/ubuntu-udd
DIR=$BASE/ubuntu-changes-unsplit
OUTPUT_DIR=$BASE/ubuntu-changes

rsync -aqz lists.ubuntu.com::changes-mboxes $DIR

for i in $DIR/*.mbox
do
	$BASE/split-ubuntu-mbox $i >/dev/null
done

make -s -C $OUTPUT_DIR
